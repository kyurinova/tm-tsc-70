package ru.tsc.kyurinova.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}