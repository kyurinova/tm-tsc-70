package ru.tsc.kyurinova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.model.User;

import java.util.Optional;

@Repository
@Scope("prototype")
public interface UserRepository extends AbstractRepository<User> {

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    void deleteById(@NotNull String id);

    void deleteByLogin(@NotNull String login);

    Optional<User> findById(@NotNull String id);

    long count();

}
