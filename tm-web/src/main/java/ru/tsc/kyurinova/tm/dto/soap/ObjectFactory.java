package ru.tsc.kyurinova.tm.dto.soap;

import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public ProjectDTO createProjectDTO() {
        return new ProjectDTO();
    }

    public ProjectFindAllRequest createProjectFindAllRequest() {
        return new ProjectFindAllRequest();
    }

    public ProjectFindAllResponse createProjectFindAllResponse() {
        return new ProjectFindAllResponse();
    }

    public ProjectAddRequest createProjectAddRequest() {
        return new ProjectAddRequest();
    }

    public ProjectAddResponse createProjectAddResponse() {
        return new ProjectAddResponse();
    }

    public ProjectSaveRequest createProjectSaveRequest() {
        return new ProjectSaveRequest();
    }

    public ProjectSaveResponse createProjectSaveResponse() {
        return new ProjectSaveResponse();
    }

    public ProjectFindByIdRequest createProjectFindByIdRequest() {
        return new ProjectFindByIdRequest();
    }

    public ProjectFindByIdResponse createProjectFindByIdResponse() {
        return new ProjectFindByIdResponse();
    }

    public ProjectExistsByIdRequest createProjectExistsByIdRequest() {
        return new ProjectExistsByIdRequest();
    }

    public ProjectExistsByIdResponse createProjectExistsByIdResponse() {
        return new ProjectExistsByIdResponse();
    }

    public ProjectCountRequest createProjectCountRequest() {
        return new ProjectCountRequest();
    }

    public ProjectCountResponse createProjectCountResponse() {
        return new ProjectCountResponse();
    }

    public ProjectDeleteByIdRequest createProjectDeleteByIdRequest() {
        return new ProjectDeleteByIdRequest();
    }

    public ProjectDeleteByIdResponse createProjectDeleteByIdResponse() {
        return new ProjectDeleteByIdResponse();
    }

    public ProjectDeleteRequest createProjectDeleteRequest() {
        return new ProjectDeleteRequest();
    }

    public ProjectDeleteResponse createProjectDeleteResponse() {
        return new ProjectDeleteResponse();
    }

    public ProjectDeleteAllRequest createProjectDeleteAllRequest() {
        return new ProjectDeleteAllRequest();
    }

    public ProjectDeleteAllResponse createProjectDeleteAllResponse() {
        return new ProjectDeleteAllResponse();
    }

    public TaskFindAllRequest createTaskFindAllRequest() {
        return new TaskFindAllRequest();
    }

    public TaskFindAllResponse createTaskFindAllResponse() {
        return new TaskFindAllResponse();
    }

    public TaskDTO createTaskDTO() {
        return new TaskDTO();
    }

    public TaskFindAllByProjectIdRequest createTaskFindAllByProjectIdRequest() {
        return new TaskFindAllByProjectIdRequest();
    }

    public TaskFindAllByProjectIdResponse createTaskFindAllByProjectIdResponse() {
        return new TaskFindAllByProjectIdResponse();
    }

    public TaskAddRequest createTaskAddRequest() {
        return new TaskAddRequest();
    }

    public TaskAddResponse createTaskAddResponse() {
        return new TaskAddResponse();
    }

    public TaskSaveRequest createTaskSaveRequest() {
        return new TaskSaveRequest();
    }

    public TaskSaveResponse createTaskSaveResponse() {
        return new TaskSaveResponse();
    }

    public TaskFindByIdRequest createTaskFindByIdRequest() {
        return new TaskFindByIdRequest();
    }

    public TaskFindByIdResponse createTaskFindByIdResponse() {
        return new TaskFindByIdResponse();
    }

    public TaskExistsByIdRequest createTaskExistsByIdRequest() {
        return new TaskExistsByIdRequest();
    }

    public TaskExistsByIdResponse createTaskExistsByIdResponse() {
        return new TaskExistsByIdResponse();
    }

    public TaskCountRequest createTaskCountRequest() {
        return new TaskCountRequest();
    }

    public TaskCountResponse createTaskCountResponse() {
        return new TaskCountResponse();
    }

    public TaskDeleteByIdRequest createTaskDeleteByIdRequest() {
        return new TaskDeleteByIdRequest();
    }

    public TaskDeleteByIdResponse createTaskDeleteByIdResponse() {
        return new TaskDeleteByIdResponse();
    }

    public TaskDeleteRequest createTaskDeleteRequest() {
        return new TaskDeleteRequest();
    }

    public TaskDeleteResponse createTaskDeleteResponse() {
        return new TaskDeleteResponse();
    }

    public TaskDeleteAllRequest createTaskDeleteAllRequest() {
        return new TaskDeleteAllRequest();
    }

    public TaskDeleteAllResponse createTaskDeleteAllResponse() {
        return new TaskDeleteAllResponse();
    }

}
