package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getDBUser();

    @NotNull
    String getDBPassword();

    @NotNull
    String getDBUrl();

    @NotNull
    String getDBDriver();

    @NotNull
    String getDBSqlDialect();

    @NotNull
    String getDBHbm2ddlAuto();

    @NotNull
    String getDBShowSql();

    @NotNull
    String getDBFormatSql();

    @NotNull
    String getDBSecondLevelCache();

    @NotNull
    String getDBCacheRegionFactory();

    @NotNull
    String getDBUseQueryCache();

    @NotNull
    String getDBUseMinPuts();

    @NotNull
    String getDBCacheRegionPrefix();

    @NotNull
    String getDBCacheProviderConfig();

}
