package ru.tsc.kyurinova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tsc.kyurinova.tm.api.endpoint.AuthEndpoint;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;
import ru.tsc.kyurinova.tm.model.Result;
import ru.tsc.kyurinova.tm.repository.DTO.UserDTORepository;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


@RestController
@RequestMapping("/api/auth")
@WebService(endpointInterface = "ru.tsc.kyurinova.tm.api.endpoint.AuthEndpoint")
public class AuthRestEndpointImpl implements AuthEndpoint {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDTORepository userDTORepository;

    @Override
    @WebMethod
    @PostMapping("/login")
    public Result login(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password
    ) {
        try {
            @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            @NotNull final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (@NotNull final Exception e) {
            return new Result(e);
        }
    }

    @Override
    @WebMethod
    @GetMapping("/profile")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public UserDTO profile() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = securityContext.getAuthentication();
        @NotNull final String username = authentication.getName();
        return userDTORepository.findByLogin(username);
    }

    @Override
    @WebMethod
    @PostMapping("/logout")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}

