package ru.tsc.kyurinova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.kyurinova.tm.enumerated.Status;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_project")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "projectDto", propOrder = {
        "id",
        "name",
        "description",
        "status",
        "startDate",
        "finishDate",
        "created",
        "createdBy",
        "updated",
        "updatedBy",
        "userId"
})
@EntityListeners(AuditingEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDTO {

    private static final long serialVersionUID = 1;

    @Id
    @NotNull
    @XmlElement(required = true)
    @Column(name = "row_id")
    protected String id = UUID.randomUUID().toString();

    @NotNull
    public ProjectDTO(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public ProjectDTO(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    public ProjectDTO(@NotNull String name, @NotNull String description, @Nullable Date startDate) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
    }

    @NotNull
    @Column
    @XmlElement(required = true)
    private String name = "";

    @NotNull
    @Column(name = "descr")
    @XmlElement(required = true)
    private String description = "";

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "start_dt")
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    @Nullable
    @Column(name = "finish_dt")
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date finishDate;

    @Nullable
    @Column
    @CreatedDate
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created;

    @Column(name = "created_by")
    @NotNull
    @CreatedBy
    private String createdBy;

    @Column
    @NotNull
    @LastModifiedDate
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date updated;

    @Column(name = "updated_by")
    @NotNull
    @LastModifiedBy
    private String updatedBy;

    @Nullable
    @Column(name = "user_id")
    private String userId;

}